#pragma once
#include <MFRC522.h>
#include <SPI.h>
#include <string.h>
#include <ArduinoJson.h>
#include <HTTPClient.h>


/* Handlerklasse zum Versenden der Daten im json Format an den Zielserver. */

class DataHandler
{
private:
    /* Ändern des Zielservers des ESP32 */
    String ServerName = "http://192.168.137.119:43560";
    /* String ServerName = "https://httpbin.org/post"; */
    HTTPClient http;
    DynamicJsonDocument document{1024};
    String RequestBody;
    int httpResponseCode;

public:
    DataHandler();
    ~DataHandler();

    /* Methode zum Versenden der Daten zum Zielserver */
    void SendData(String data, String *sigareadata); 
};
