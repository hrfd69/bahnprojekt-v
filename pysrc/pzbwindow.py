import tkinter as tk
from multiprocessing import Queue
import server
from threading import Thread
# from threading import Lock

# Hauptprogramm der Serverseite 
class App(tk.Tk):
    def __init__(self, lock):
        tk.Tk.__init__(self)

        # Multithreading Lock, soll den fremden Zugriff auf Multithreading Variable verhindern, wenn diese von einem Thread verwendet wird.
        self.mutex = lock
        
        # Festlegen der Fenstergröße
        self.geometry("1280x720")
        frameLabel = tk.Frame(self, padx=40, pady =40)
        self.text = tk.Text(frameLabel, wrap='word', font='TimesNewRoman 30',
                            bg=self.cget('bg'), relief='flat')
        frameLabel.pack()
        self.text.pack() 
        
        self.queue = Queue()
        # TODO: Window needs to display handelded value
        
        # NOTE: CREATE instance with queue
        Server = server.GetData(self.queue)
        #Start thread1
        thread1 = Thread(target=Server.runserver)
        thread1.start() 

        self.displayData()
        
    def displayData(self):
        self.text.delete(1.0, 'end')
        self.text.insert('end', 'Gelesen: ')

        data = self.queue.get()
        self.text.insert('end', data['identity'])
        self.text.insert('end', " ")
        self.text.insert('end', data['TagData'])
        self.text.insert('end', " ")
        self.text.insert('end', str(data['Area']))
        self.text.insert('end', " ")
        self.text.insert('end', str(data['Signal']))
        self.text.insert('end', " ")
        self.after(100, self.displayData)