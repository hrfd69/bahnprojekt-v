#pragma once
#include <MFRC522.h>
#include <SPI.h>
#include <string.h>
#include "pzb.h"



class ReadWrite : pzb
{
private:
    byte buffer[18];
    byte bufferlen;

    byte blockaddr;
    byte writebuffer[16];

    String data;

    String AddInfo[1];

    bool cardpresent = false;

    MFRC522 mfrc522;
    MFRC522::MIFARE_Key key;
    /* Error Variable */
    MFRC522::StatusCode status; 
    /* Variable zum Identifizieren des PICC Typs */ 
    MFRC522::PICC_Type piccType;

public:
    ReadWrite(byte RST_PIN, byte SS_PIN);

    /* Funktion zum Schreiben von Informationen in Tag. */
    // Standardmäßig: Block 5 mit 0x02
    void WriteDataToBlock(byte pzb, byte signal, byte area);
    /* Auslesen von Informationen in Block 5 eines vorgehaltenen Tags */  
    void ReadDataFromBlock();
    /*  */
    void authenticate();   
    /* Überprüfen ob NFC-Tag vorhanden und anschließend auslesen des Tags  */
    void CardSetup();
    /* Ausgabe der UID des jeweiligen NFC Tags */         
    void PrintUid();
    /* Ausgabe der gelesenen Daten über die serielle Schnittstelle.  */
    void PrintData();
    /* Beenden und Zurücksetzen der Verbindung zum NFC Tag  */
    void StopConn();          
    /* Gibt zurück, ob ein NFC-Tag detektiert wurde */
    bool IsCardPresent();     

    /* Übersetzt den gelesenen Wert des PZB-Schwingkreis Typs */
    void translatePZB();
    /* Übersetzt die gelesenen Daten in die Signalnummer und die Bereichskennziffer */
    void getArea();

    /* VERALTET: Gibt die gelesenen Daten des NFC-Tags als String zurück.*/
    String returnDataFromTag();
    /* OBSOLETE: Returns Signal and Area info from some tags. -> Some have a random value */
    String* returnInfoFromTag();
};