#pragma once
#include <Arduino.h>
#include <string.h>


#define VSIG_1000 0x01
#define VSIG_500 0x04
#define HSIG 0x02

#define P_2000 0x01
#define P_1000 0x02
#define P_500 0x04


class pzb
{
    public:
    /* Die PZB beinhalt immer die Variable des jeweiligen PZB Wertes */
    String pzb; 
    /* "field" legt das Feld des zugehörigen Signals fest */
    String area;
    /* Legt den Typ fest. Wird nicht genutzt. */
    String type; 

};