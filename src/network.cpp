#include "network.h"
#include <WiFi.h>

Network::Network(const char *ssidval,const char *passval)
{
    ssid = ssidval;
    pass = passval;

    connect();
}

void Network::connect()
{
    Serial.print("Network: ");
    Serial.print(ssid);

    WiFi.begin(ssid, pass);
    Serial.println("Connecting");
    int i = 0;
    while (WiFi.status() != WL_CONNECTED)
    {
        delay(500);
        Serial.print(".");
        
        /* Wait for 5 Loops, if not connected exit */
        if (i >= 5){
            Serial.println("WLAN not connected. Exiting.");
            return;
        }
        i++;
    }
    Serial.println("");
    Serial.println("WiFi verbunden");
    Serial.println("IP Adress is: ");
    Serial.println(WiFi.localIP());
    Serial.println("DNS: ");
    Serial.println(WiFi.dnsIP());
};