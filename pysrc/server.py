from flask import Flask, Response
from flask import request
import base64
from tkinter import *
import json
from threading import Thread
from threading import Lock
import subprocess as sb
from socket import gethostname, gethostbyname
from multiprocessing import Queue


def post_actions(data):
    return "200"


class GetData:
    def __init__(self, que):
        self.server = Flask(__name__)
        self.que = que
        # self.mutex = Lock()
        self.server.add_url_rule(
            '/', view_func=self.response, methods=['GET', 'POST'])

    # def add_endpoint():
        #self.server.add_url_rule("/", methods=['GET', 'POST'])
        # return
    def run(self):
        self.server.run()

    # def runserver(self):
   # @server.route("/")
    def response(self):
        if request.method == 'POST':
            if request.data:
                # print(request.data)
                rcv_data = json.loads(request.data)
                rsp = post_actions(rcv_data)
                #self.que = rcv_data
                # Put data into queue
                self.que.put(rcv_data)
                print(rcv_data['identity'])
                print(rcv_data['TagData'])
                print(rcv_data['Area'])
                print(rcv_data['Signal'])
            if rsp:
                return rsp
            else:
                return '200'
        else:
            return '404'
        # return

    def runserver(self):
        #Change to used local network name
        self.server.run("192.168.137.119", port='43560')
        return

class EndpointAction:
    def __init__(self, action):
        self.action = action
        self.response = Response(status=200, headers={})

    def __call__(self, *args):
        self.action()
        return self.response


class FlaskWrapper:
    app = None

    def __init__(self, name):
        self.app = Flask(name)

    def run(self):
        self.app.run()
    # def add_endpoint(self, endpoint=None, endpoint_name = None, handler = None, self.app.add_url_rule(endpoint, endpoint_name, Endpoint_Action(handler)))







