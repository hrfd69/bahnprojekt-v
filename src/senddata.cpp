#include "senddata.h"
#include <MFRC522.h>
#include <SPI.h>
#include <string.h>
#include <ArduinoJson.h>
#include <HTTPClient.h>

DataHandler::DataHandler()
{
    http.begin(ServerName);
    http.addHeader("Content-Type", "application/json");
    document["identity"] = "BR218";
}

DataHandler::~DataHandler()
{
    http.end();
}

void DataHandler::SendData(String data, String* sigareadata)
{
    document["TagData"] = data;
    document["Area"] = sigareadata[0];
    document["Signal"] = sigareadata[1];

    serializeJson(document, RequestBody);
    serializeJsonPretty(document, Serial);

    httpResponseCode = http.POST(RequestBody);

    delay(100);

    if (httpResponseCode == 200) /* Setze auf == 200, vorher > 0, httpResponseCode ist int. Das Casting kann fehlschlagen. */ 
    {
        String response = http.getString();

        Serial.println(httpResponseCode);
        Serial.println(response);
    }
    else
    {
        Serial.printf("Error occurred while sending HTTP POST\n");
    }
}
