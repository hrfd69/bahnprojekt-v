#pragma once
#include <MFRC522.h>
#include <WiFi.h>

class Network{
    private:
    /* SSID and Password for Network */
        const char* ssid;
        const char* pass;


    public:
        Network(const char* ssidval,const char* passval);
        void connect();

};
