#include <SPI.h>
#include <MFRC522.h>
#include "readwrite.h"
#include "network.h"
#include "senddata.h"

/* SPI wird fix zugewiesen. Entnahme aus Datenblatt des jeweiligen Controllers */

/* --------------------------- */

/* Definieren des Slave Select Pins, zur Auswahl des jeweiligen Slaves am Bus */
#define SS_PIN 5   
/* Der Reset PIN zur  */
#define RST_PIN 27


/* Zuweisung der Klassenadressen im Speicher */
ReadWrite *RW = NULL;
Network *NT = NULL;
DataHandler *Handler = NULL;

void setup()
{
  /* Passwort und Netzwerk-SSID zu Testzwecken, hier den Namen und das Passwort des WLAN Netzwerkes ändern */
  const char *ssid = "WLAN-Notebook";
  const char *pass = "12345678";


  /* Initialisieren der seriellen Kommunikation über USB */
  Serial.begin(9600);
  
  /* SPI Bus starten */
  SPI.begin();
  
  /* Aufsetzen der ReadWrite Klasse und Network Klasse an vorher zugewiesener Speicheradresse */
  RW = new ReadWrite(RST_PIN, SS_PIN);
  NT = new Network(ssid, pass);
  

  /* Serial.println("Scan a MIFARE 1K Tag to write data..."); */
}

/* #### Hauptprogramm #### */
/* --------------------------- */

void loop()
{
  /* Warten auf NFC Karte */
  RW->CardSetup();

  if (!RW->IsCardPresent())
  {
    delay(10);
    return;
  }

 /*  RW->PrintUid(); */

  /* Schreibklasse für NFC-Tag. Nur zu Testzwecken. Wird im Programm nicht verwendet. Setzt den Wert an Adresse 5 auf 0x02 */ 
  /* RW->WriteDataToBlock(); */

  /* Auslesen der Daten auf dem Tag */
  RW->ReadDataFromBlock();

  /* Serielle Ausgabe der gelesenen Information, nur zu Debugging Zwecken. */
  RW->PrintData();

  /* Übersetzen der jeweilig gelesenene Informationen in den PZB Befehl und die jeweilige Signalnummer und Bereichskennziffer */
  RW->translatePZB();
  RW->getArea();
  
  /* NETZWERK */
  /* Starten des Handlers und erstellen der JSON Information */
  Handler = new DataHandler();
  Handler->SendData(RW->returnDataFromTag(), RW->returnInfoFromTag());
  delete Handler;
  Handler = NULL;

  /* Rücksetzen der NFC-Verbindung */
  RW->StopConn();

  delay(100);
}