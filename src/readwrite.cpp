#include "readwrite.h"
#include <Arduino.h>
#include <MFRC522.h>
#include <SPI.h>

ReadWrite::ReadWrite(byte RST_PIN, byte SS_PIN)
{
  bufferlen = 18;
  mfrc522 = MFRC522{SS_PIN, RST_PIN};
  mfrc522.PCD_Init();

  key = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};

  blockaddr = 5;

  /* strcpy((char *)writebuffer, "DataonTag"); */
  /* Set signal data with PZB in bit 1 and the signal id on tag in buffer */
  writebuffer[0] = P_500 | HSIG;

  /* check if bit is set with mask. */
  Serial.println("Bit is set.");
  // Serial.print(writebuffer[0], BIN);
  if (writebuffer[0] & P_1000 > 0)
  {
  }
}

void ReadWrite::WriteDataToBlock(byte pzb = 0x01, byte signal = 0x00, byte area = 0x00)
{
  /* authenticate(); */

  /* Write data to the block */
  Serial.print("\n");
  Serial.println("Writing to Data Block...");

/* Variablen in Block setzen */
  writebuffer[0] = pzb;
  writebuffer[1] = signal;
  writebuffer[2] = area;

/* Schreibvorgang */
  status = mfrc522.MIFARE_Write(0x05, writebuffer, 16);
  if (status != MFRC522::STATUS_OK)
  {
    Serial.print("Writing to Block failed: ");
    Serial.println(mfrc522.GetStatusCodeName(status));
    return;
  }
  else
  {
    Serial.println("Data was written into Block successfully");
  }
}

void ReadWrite::ReadDataFromBlock()
{
  /* Not used. Causes errors. Not working in System */
  /* authenticate(); */

  status = mfrc522.MIFARE_Read(blockaddr, buffer, &bufferlen);
  Serial.print("Bufferlength nach beschreiben: ");
  Serial.println(bufferlen);
  /* Reset buffer to keep reading new tags */
  bufferlen = 18;
  Serial.print("Bufferlength after reset: ");
  Serial.println(bufferlen);
  
  if (status != MFRC522::STATUS_OK)
  {
    Serial.print("Reading failed: ");
    Serial.println(mfrc522.GetStatusCodeName(status));
    return;
  }
  else
  {
    Serial.println("Block was read successfully");
    for (int i = 0; i < 17; i++)
    {
      data += String(buffer[i]); // TODO: Set to translate function, to send data to sever
    }
  }
  /* buffer[17] = '\0'; */
}

void ReadWrite::authenticate()
{
  status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, blockaddr, &key, &(mfrc522.uid));

  if (status != MFRC522::STATUS_OK)
  {
    Serial.print("Authentication failed: ");
    Serial.println(mfrc522.GetStatusCodeName(status));
  }
  else
  {
    Serial.println("Got it.");
  }
}

void ReadWrite::CardSetup()
{
  cardpresent = false;
  /* Look for new card, if none found, repeat. Only true if card is found */
  if (!mfrc522.PICC_IsNewCardPresent())
  {
    // Serial.println("No Card"); // Do not uncomment, it's getting messy
    return;
  }
  if (!mfrc522.PICC_ReadCardSerial())
  {
    Serial.println("Reading UID");
    /* Added delay function to stop multiple Error readings. */
    delay(10); 
    return;
  }
  Serial.println("Card present!");
  cardpresent = true;
}

bool ReadWrite::IsCardPresent()
{
  return cardpresent;
}

void ReadWrite::PrintUid()
{
  Serial.print(F("Card UID: "));
  for (byte i = 0; i < mfrc522.uid.size; i++)
  {
    Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
    Serial.print(mfrc522.uid.uidByte[i], HEX);
  }
  Serial.print("\n");

  Serial.print("PICC Type: ");
  piccType = mfrc522.PICC_GetType(mfrc522.uid.sak);
  Serial.println(mfrc522.PICC_GetTypeName(piccType));

  Serial.print("\n");
}

void ReadWrite::PrintData()
{
  Serial.print("\n");
  Serial.print("Data in Block:");
  Serial.print(blockaddr);
  Serial.print(" --> ");
  for (int j = 0; j < 1; j++)
  {
    Serial.print(buffer[j], HEX);
  }
  Serial.print("\n");
  Serial.println(buffer[0], DEC);
}

String ReadWrite::returnDataFromTag()
{
  return pzb;
};

String *ReadWrite::returnInfoFromTag()
{
  return AddInfo;
}

void ReadWrite::StopConn()
{
  mfrc522.PICC_HaltA();      // halt PICC
  mfrc522.PCD_StopCrypto1(); // stop encryption on PCD
}

void ReadWrite::translatePZB()
{

  if (buffer[0] != 0x00)
  /* Serial.println(buffer[0], HEX); // NOTE: For Debugging
  Serial.print("PZB Info ist declared als: ");
  Serial.print(P_1000, HEX);
  Serial.print("\n"); */
  {
    if (buffer[0] == P_2000)
    {
      pzb = "2000Hz";
    }
    else if (buffer[0] == P_1000)
    {
      pzb = "1000Hz";
    }
    else if (buffer[0] == P_500)
    {
      pzb = "500Hz";
    }
    else
    {
      pzb = "noVal";
    }
  }
}


void ReadWrite::getArea()
{
  byte areainfo = buffer[1];
  byte signalinfo = buffer[2];

  /* NOTE: For Debugging purposes */
  /* Serial.print("Areainfo: ");
  Serial.println(areainfo, HEX);
  Serial.print("Signalnr.: ");
  Serial.println(signalinfo, HEX);
 */
  AddInfo[0] = String(signalinfo);
  AddInfo[1] = String(areainfo);
}