import server
import subprocess as sb
import tkinter as tk
from threading import Thread
from threading import Lock
from multiprocessing import Queue
import pzbwindow


def main():
    

    lock = Lock()
    
    # Hauptprogrammm in App Klasse
    pzbhost = pzbwindow.App(lock)

    # Hauptprogramm
    pzbhost.mainloop()

    return 



if __name__ == "__main__":
    main()